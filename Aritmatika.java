import java.util.Scanner;

public class Aritmatika {
    public static void main(String[] args) {
        System.out.println("Hello World");

        Scanner in = new Scanner(System.in);
        System.out.print("Bilangan 1 : ");
        int bil1 = in.nextInt();
        System.out.print("Bilangan 2 : ");
        int bil2 = in.nextInt();

        System.out.println("Hasil penjumlahan : " + penjumlahan(bil1, bil2));
    }

    private static int penjumlahan(int bil1, int bil2) {
        return bil1+bil2;
    }
}
